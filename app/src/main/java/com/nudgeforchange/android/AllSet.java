package com.nudgeforchange.android;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.android.nudgeforchange.R;

public class AllSet extends Activity {
    Button mCloseButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_set);
        mCloseButton = (Button) findViewById(R.id.b_all_set_close);
        mCloseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AllSet.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }

    public void onBackPressed() {
        moveTaskToBack(true);
    }
}
