package com.nudgeforchange.android;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.android.nudgeforchange.R;
import com.mindorks.placeholderview.SwipeDecor;
import com.mindorks.placeholderview.SwipePlaceHolderView;

import java.util.ArrayList;
import java.util.List;

public class Questions extends Activity {

    // SharedPreferences will be saved in a file named MyPrefsFile
    public static final String PREFS_NAME = "NudgeForChangePrefs";

    private SwipePlaceHolderView mSwipeView;
    private Context mContext;

    private int environment;
    private int female;
    private int race;
    private int lgbt;
    private int wage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questions);
        mSwipeView = (SwipePlaceHolderView)findViewById(R.id.swipeView);
        mContext = getApplicationContext();

        // get SharedPreferences from file named MyPrefsFile
        SharedPreferences sp = getSharedPreferences(PREFS_NAME, 0);
        environment = sp.getInt("environment", 0);
        female = sp.getInt("female", 0);
        race = sp.getInt("race", 0);
        lgbt = sp.getInt("lgbt", 0);
        wage = sp.getInt("wage", 0);

        // write default values to Shared Prefs
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt("environment", environment);
        editor.putInt("female", female);
        editor.putInt("race", race);
        editor.putInt("lgbt", lgbt);
        editor.putInt("wage", wage);
        editor.commit();

        mSwipeView.getBuilder()
                .setDisplayViewCount(5)
                .setSwipeDecor(new SwipeDecor()
                        .setPaddingTop(20)
                        .setRelativeScale(0.01f)
                        .setSwipeInMsgLayoutId(R.layout.tinder_swipe_in_msg_view)
                        .setSwipeOutMsgLayoutId(R.layout.tinder_swipe_out_msg_view));

        List<Integer> images = new ArrayList<>();
        images.add(R.drawable.environment);
        images.add(R.drawable.female);
        images.add(R.drawable.race);
        images.add(R.drawable.lgbt);
        images.add(R.drawable.wage);

        for (int i = 0; i < images.size(); i++) {
            mSwipeView.addView(new TinderCard(mContext, images.get(i), mSwipeView));
        }

        findViewById(R.id.rejectBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSwipeView.doSwipe(false);
                if (mSwipeView.getChildCount() != 0) {
                    mSwipeView.doSwipe(true);

                    int numCards = mSwipeView.getChildCount();
                    switch (numCards) {
                        case 5:
                            environment = 0;
                            saveNumber("environment", environment);
                            break;
                        case 4:
                            female = 0;
                            saveNumber("female", female);
                            break;
                        case 3:
                            race = 0;
                            saveNumber("race", race);
                            break;
                        case 2:
                            lgbt = 0;
                            saveNumber("lgbt", lgbt);
                            break;
                        case 1:
                            wage = 0;
                            saveNumber("wage", wage);
                            getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit()
                                    .putBoolean("isQuestionsAnswered", true).apply();
                            Intent intent = new Intent(mContext, OneLastThing.class);
                            startActivity(intent);
                    }
                }
                else {
                    Intent intent = new Intent(mContext, OneLastThing.class);
                    startActivity(intent);
                }
            }
        });

        findViewById(R.id.acceptBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSwipeView.getChildCount() != 0) {
                    mSwipeView.doSwipe(true);
                    int numCards = mSwipeView.getChildCount();
                    switch (numCards) {
                        case 5:
                            environment = 1;
                            saveNumber("environment", environment);
                            break;
                        case 4:
                            female = 1;
                            saveNumber("female", female);
                            break;
                        case 3:
                            race = 1;
                            saveNumber("race", race);
                            break;
                        case 2:
                            lgbt = 1;
                            saveNumber("lgbt", lgbt);
                            break;
                        case 1:
                            wage = 1;
                            saveNumber("wage", wage);
                            getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit()
                                    .putBoolean("isQuestionsAnswered", true).apply();
                            Intent intent = new Intent(mContext, OneLastThing.class);
                            startActivity(intent);
                    }
                }
                else {
                    Intent intent = new Intent(mContext, OneLastThing.class);
                    startActivity(intent);
                }
            }
        });
    }
    public void saveNumber(String name, int value) {

        // Code to save int to SharedPreferences
        SharedPreferences sp = getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = sp.edit();

        // Alternate way to write the lines of code above in one line
        // SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, 0).edit();

        editor.putInt(name, value);
        editor.commit();
        Log.d("EVENT", "Saved " + name + " to shared prefs. Value = " + sp.getInt(name, -1));
    }
}
