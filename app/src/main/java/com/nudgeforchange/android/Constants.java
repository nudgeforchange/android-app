package com.nudgeforchange.android;

/**
 * Created by John Vehikite on 7/12/2017.
 */

public final class Constants {
    private Constants() {
    }
    public static final String PACKAGE_NAME = "com.nudgeforchange.android";
    public static final String BROADCAST_ACTION = PACKAGE_NAME + ".BROADCAST_ACTION";
    public static final String ACTIVITY_EXTRA = PACKAGE_NAME + ".ACTIVITY_EXTRA";

}
