package com.nudgeforchange.android;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.android.nudgeforchange.R;

/**
 * Created by John Vehikite on 2/20/2017.
 */

public class TabThree extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tab3_big_change, container, false);
        Button mButton = (Button) rootView.findViewById(R.id.b_instructions_begin);
        mButton.setOnClickListener(mButtonClickListener);
        return rootView;
    }

    private View.OnClickListener mButtonClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(getActivity(), Questions.class);
            startActivity(intent);
        }
    };
}
