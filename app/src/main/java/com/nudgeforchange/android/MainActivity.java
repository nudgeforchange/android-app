package com.nudgeforchange.android;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.android.nudgeforchange.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    public static final String PREFS_NAME = "NudgeForChangePrefs";
    Button mButton;

    protected static final String TAG = "MainActivity";
    protected GoogleApiClient mGoogleApiClient;
    protected Location mLastLocation;
    protected LocationRequest mLocationRequest;
    private Geofencing mGeofencing;
    //protected ActivityDetectionBroadcastReceiver mBroadcastReceiver;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //mBroadcastReceiver = new ActivityDetectionBroadcastReceiver();

        // Save Best Buy's ID to prefs
        // TODO: get data from Nudge API
        SharedPreferences sp = getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("bb-id", "ChIJ5esF4t9eVFMRUJc2PT9IDVo");

        // the place id that was commented out was used for testing a physical device
        //editor.putString("bb-id", "ChIJl6afzvp3hYARULOcgiTY7rU");

        editor.putInt("bb-environment", 9);
        editor.putInt("bb-female", 7);
        editor.putInt("bb-race", 2);
        editor.putInt("bb-lgbt", 10);
        editor.putInt("bb-wage", 4);
        editor.apply();
        mButton = (Button) findViewById(R.id.b_main_reset_answers);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetValues();
                onResume();
            }
        });
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .enableAutoManage(this, this)
                .build();

        mGeofencing = new Geofencing(this, mGoogleApiClient);
    }

    @Override
    protected void onResume() {
        super.onResume();
        boolean isQuestionsAnswered;
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        if (!settings.contains("isQuestionsAnswered")) {
            isQuestionsAnswered = settings.getBoolean("isQuestionsAnswered", false);
            startActivity(new Intent(MainActivity.this, TabbedInstructions.class));
        }
        isQuestionsAnswered = settings.getBoolean("isQuestionsAnswered", false);
        if (isQuestionsAnswered && (ActivityCompat.checkSelfPermission(MainActivity.this,
                android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)) {
            Log.d(TAG, "Launch steps completed previously");
            compareScores();
        }
        if (isQuestionsAnswered && (ActivityCompat.checkSelfPermission(MainActivity.this,
                android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {
            startActivity(new Intent(MainActivity.this, OneLastThing.class));
        } else if (!isQuestionsAnswered && (ActivityCompat.checkSelfPermission(MainActivity.this,
                android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)) {
            startActivity(new Intent(MainActivity.this, Questions.class));
        }
    }

    public void onBackPressed() {
        moveTaskToBack(true);
    }

    private void resetValues() {
        // TODO: Clean up this code
        int environment, female, race, lgbt, wage;

        SharedPreferences sp = getSharedPreferences(PREFS_NAME, 0);

        if (!sp.contains("environment")) {
            environment = sp.getInt("environment", 0);
            female = sp.getInt("female", 0);
            race = sp.getInt("race", 0);
            lgbt = sp.getInt("lgbt", 0);
            wage = sp.getInt("wage", 0);
        }

        List<String> keys = new ArrayList<>();
        keys.add("environment");
        keys.add("female");
        keys.add("race");
        keys.add("lgbt");
        keys.add("wage");

        SharedPreferences.Editor editor = sp.edit();

        for (String key : keys) {
            // zero out values
            editor.putInt(key, 0);

        }
        editor.putBoolean("isQuestionsAnswered", false);
        editor.apply();
        Log.d(TAG, "Values reset");
        Log.d(TAG, "isQuestionsAnswered = " + sp.getBoolean("isQuestionsAnswered", false));
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.i(TAG, "API Client Connection Successful!");
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    class RefreshPlacesData extends AsyncTask<Void, Void, Void> {
        PlaceBuffer placeBuffer;

        @Override
        protected Void doInBackground(Void... voids) {
            SharedPreferences sp = getSharedPreferences(PREFS_NAME, 0);
            String id = sp.getString("bb-id", "");
            // pass in Google API client and list (converted to an array
            // PlaceBuffer is returned
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi.getPlaceById(mGoogleApiClient,
                    id);
            placeBuffer = placeResult.await();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Log.d("refreshPlacesData", "Name: " + placeBuffer.get(0).getName().toString());
            Log.d("refreshPlacesData", "Address: " + placeBuffer.get(0).getAddress().toString());
            mGeofencing.updateGeofencesList(placeBuffer);
            mGeofencing.registerAllGeofences();
        }
    }

    private void compareScores() {
        SharedPreferences sp = getSharedPreferences(PREFS_NAME, 0);
        if ((sp.getInt("environment", -1) == 1) && (sp.getInt("bb-environment", 20) < 6) ||
                (sp.getInt("female", -1) == 1) && (sp.getInt("bb-female", 20) < 6) ||
                (sp.getInt("race", -1) == 1) && (sp.getInt("bb-race", 20) < 6) ||
                (sp.getInt("lgbt", -1) == 1) && (sp.getInt("bb-lgbt", 20) < 6) ||
                (sp.getInt("wage", -1) == 1) && (sp.getInt("bb-wage", 20) < 6)) {
            new RefreshPlacesData().execute();
        } else {
            Log.d(TAG, "No low scores in user's value set");
        }
    }
}
