package com.nudgeforchange.android;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.nudgeforchange.R;

/**
 * Created by John Vehikite on 2/20/2017.
 */

public class TabOne extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tab1_ready_to_change, container, false);
        return rootView;
    }
}
